module.exports = function(grunt) {
    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            // dist: {
            //     // the files to concatenate
            //     src: [
            //         'bower_components/nunjucks/browser/nunjucks-slim.min.js',
            //         'build/site.js'
            //     ],
            //     // the location of the resulting JS file
            //     dest: 'www/js/site.js'
            // },
            panel: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/nouislider/jquery.nouislider.min.js',
                    'frontend/panel/panel.js'
                ],
                dest: 'app/static/js/panel.js'
            },
            panel_css: {
                src: [
                    'bower_components/nouislider/jquery.nouislider.css',
                ],
                dest: "app/static/css/panel.css"
            }
        },

        nunjucks: {
            precompile: {
                baseDir: 'frontend/templates/browser',
                src: 'frontend/templates/browser/*',
                dest: 'build/templates.js',
                options: {
                    // env: require('./app/lib/nunjucks/nunjucks'),
                    name: function(filename) {
                        return filename.replace(/\.j2$/, "");
                    }
                }
            }
        },

        browserify: {
            application: {
                files: {
                    'build/site.js': [
                        'frontend/js/components/**/*.js',
                        'frontend/js/site.js'
                    ]
                }
            }
        },

        watch: {
            js: {
                options: {
                    nospawn: true,
                    livereload: true
                },
                files: [
                    "frontend/js/**/*.js",
                    "templates/client/**/*.j2"
                ],
                tasks: [
                    "js"
                ]
            },
            sass: {
                options: {
                    nospawn: true,
                    livereload: true
                },
                files: [
                    "frontend/sass/**/*.scss"
                ],
                tasks: [
                    "sass"
                ]
            }
        },

        clean: {
            js: "build"
        },

        sass: {
            dev: {
                files: {
                    "www/css/site.css": "frontend/sass/screen.scss",
                    "www/css/style-guide.css": "frontend/sass/style-guide.scss",
                },
                options: {
                    sourceComments: 'map'
                }
            }
        },

        nodemon: {
            dev: {
                script: 'app.js',
                options: {
                    cwd: __dirname,
                    ignore: [
                        'node_modules/**',
                        'bower_components/**'
                    ],
                    ext: 'js,json'
                }
            }
        }

    });

    /**
     * The cool way to load your grunt tasks
     * --------------------------------------------------------------------
     */
    Object.keys( pkg.devDependencies ).forEach( function( dep ){
        if( dep.substring( 0, 6 ) === 'grunt-' ) grunt.loadNpmTasks( dep );
    });


    grunt.registerTask("default", [
        "nodemon"
    ]);

    grunt.registerTask("js", [
        "browserify",
        "nunjucks",
        "concat",
        "clean:js"
    ]);





};