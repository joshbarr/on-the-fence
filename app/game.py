from flask import Blueprint, render_template, abort, url_for
from jinja2 import TemplateNotFound
import flask.ext.security as security
from models.Candidate import Candidate
from models.PolicyArea import PolicyArea

bp = Blueprint('game', __name__, template_folder='templates')

@bp.route('/')
def index():
    policy_areas = PolicyArea.query.all()
    return render_template('app/play.j2', policy_areas = policy_areas)

@bp.route('/policy/<policy_id>')
def policy(policy_id):
    policy_area = PolicyArea.query.filter_by(id=policy_id).first_or_404()
    return render_template("app/policy.j2", policy=policy_area)

@bp.route('/results/')
def results():
    candidates = Candidate.query.all()
    return render_template("app/results.j2", candidates = candidates)
