from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, json, escape, make_response, send_from_directory, after_this_request

import flask.ext.security as security
from app import app, db, PolicyArea, Candidate

Policies = PolicyArea.PolicyArea
Candidates = Candidate.Candidate


@app.route('/')
def index():
    return render_template("index.j2")


@app.route('/about/')
def about():
    return render_template("about.j2")


@app.route('/sign-out')
def logout():
    return render_template("security/sign-out.j2")



@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.j2'), 404


@app.errorhandler(403)
def page_not_found(e):
    return render_template('403.j2'), 403


@app.errorhandler(405)
def page_not_found(e):
    return render_template('405.j2'), 405