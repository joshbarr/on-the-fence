from app import db

class Feedback(db.Model):
    __tablename__ = 'feedback'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    text = db.Column(db.Text())
    date = db.Column(db.DateTime())
    
    def __unicode__(self):

    	text = self.text

    	if text:
    		text = text[:30]

        return "%s: %s" % (text, self.date)