from app import db


class PanelQuestionaire(db.Model):
    __tablename__ = 'panel_questionaire'
    __table_args__ = { 'sqlite_autoincrement': True }

    id = db.Column(db.Integer, primary_key=True, nullable=True)
    
    panel_member_id = db.Column(db.Integer, db.ForeignKey('user.id'), unique=True)
    panel_member = db.relationship('User', backref='user')
    
    responses = db.relationship("PanelResponse", backref='panel_questionaire', lazy='dynamic')
    exclude_these_results = db.Column(db.Boolean)


    def __unicode__(self):
        return "Questionaire: %s" % (self.panel_member)