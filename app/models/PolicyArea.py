from app import db

class PolicyArea(db.Model):
    __tablename__ = 'policy_area'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128))
    hot_topic = db.Column(db.Boolean())
    statements = db.relationship("Statement", backref="policy_area")

    def __unicode__(self):
        return self.name