from app import db


class PanelResponse(db.Model):
    __tablename__ = 'panel_response'
    __table_args__ = { 'sqlite_autoincrement': True }

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True, nullable=True)
    
    panel_questionaire_id = db.Column(db.Integer, db.ForeignKey('panel_questionaire.id'))

    policy_area_id = db.Column(db.Integer, db.ForeignKey('policy_area.id'))
    policy_area = db.relationship('PolicyArea', backref='policy_area')

    candidate_id = db.Column(db.Integer, db.ForeignKey('candidate.id'))
    candidate = db.relationship('Candidate', backref='candidate')

    value = db.Column(db.Float)

    def __unicode__(self):
        return "Response"