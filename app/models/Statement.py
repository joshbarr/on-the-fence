from app import db

class Statement(db.Model):
    __tablename__ = 'statements'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    policy_area_id = db.Column(db.Integer, db.ForeignKey('policy_area.id'))
    bias_id = db.Column(db.Integer, db.ForeignKey('bias.id'))
    bias = db.relationship("Bias")
    draft = db.Column(db.Boolean())
    text = db.Column(db.Text())

    def __unicode__(self):
        return "%s" % (self.text)