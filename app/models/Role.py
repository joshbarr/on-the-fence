from app import db
from flask.ext.security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), unique=True)
    title = db.Column(db.String(128))
    description = db.Column(db.Text())
    disabled = db.Column(db.Boolean())

    def __unicode__(self):
        return "%s" % (self.name)