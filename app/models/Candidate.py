from app import db

class Candidate(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128))
    blurb = db.Column(db.Text())
    image = db.Column(db.String(256))
    url = db.Column(db.String(512))
    confirmed = db.Column(db.Boolean())
    
    def __unicode__(self):
        return self.name