from app import db

class Bias(db.Model):
    __tablename__ = 'bias'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128))
    description = db.Column(db.Text())
    x_coordinate = db.Column(db.Integer())
    y_coordinate = db.Column(db.Integer())

    def __unicode__(self):
        return "%s" % (self.name)