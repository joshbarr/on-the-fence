from app import app, db, flask_login
from flask.ext.admin import Admin, BaseView, expose
from flask.ext.admin import helpers, expose
from flask.ext.admin.contrib.sqla import ModelView
import flask.ext.admin.contrib.sqla as sqla
from flask.ext import admin, login
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, json, escape, make_response, send_from_directory, after_this_request
import flask.ext.security as flask_security

from models.User import User
from models.Statement import Statement
from models.Candidate import Candidate
from models.PolicyArea import PolicyArea
from models.PanelMember import PanelMember
from models.Role import Role
from models.Feedback import Feedback
from models.PanelQuestionaire import PanelQuestionaire
from models.Bias import Bias
from models.PanelResponse import PanelResponse

# Create customized index view class that handles login & registration
class OnTheFenceAdmin(admin.AdminIndexView):

    @expose('/')
    @flask_security.login_required
    @flask_security.roles_accepted("admin", "editor")
    def index(self):
        if not login.current_user.is_authenticated():
            return redirect(url_for('.login_view'))

        return super(OnTheFenceAdmin, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        print "foo"
        return redirect("../login")

    @expose('/register/', methods=('GET', 'POST'))
    def register_view(self):
        return redirect("../register")

    @expose('/logout/', methods=('GET', 'POST'))
    def logout_view(self):
        return redirect("../logout")



class Users(ModelView):

    # Override displayed fields
    column_list = ('email', 'active', 'last_login_at', 'roles')
    form_excluded_columns = ('last_login_at', 'last_login_ip', 'current_login_at', 'login_count','current_login_ip', "confirmed_at")

    def is_accessible(self):
        return login.current_user.is_authenticated()

    def __init__(self, session, **kwargs):
        # You can pass name and other parameters if you want to
        super(Users, self).__init__(User, session, **kwargs)



class PanelMembers(ModelView):
    column_list = ('email', 'active', 'last_login_at', 'disabled', 'roles')
    column_filters = ('roles', 'email')

    def is_accessible(self):
        return login.current_user.is_authenticated()
    
    def __init__(self, session, **kwargs):
        super(PanelMembers, self).__init__(User, session, **kwargs)


class StatementAdmin(sqla.ModelView):
    column_display_pk = True


class PolicyAreaAdmin(sqla.ModelView):
    inline_models = (Statement,)


class ResponseAdmin(sqla.ModelView):
    inline_models = (PanelResponse,)
    # column_formatters = dict(responses=lambda v, c, m, p: length(m.responses))
    # column_descriptions = dict(
    #     responses='PolicyAreas answered'
    # )

# Boot up admin
admin_area = Admin(
    app,
    index_view = OnTheFenceAdmin(),
    name = 'On the fence',
    base_template = 'admin/index.j2'
)

admin_area.add_view(Users(db.session))
admin_area.add_view(PolicyAreaAdmin(PolicyArea, db.session))
admin_area.add_view(StatementAdmin(Statement, db.session))
admin_area.add_view(ModelView(Bias, db.session))
admin_area.add_view(ModelView(Candidate, db.session))
admin_area.add_view(ModelView(Role, db.session))
admin_area.add_view(ModelView(Feedback, db.session))
admin_area.add_view(ResponseAdmin(PanelQuestionaire, db.session))


