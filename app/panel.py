from flask import Blueprint, render_template, abort, request, redirect, url_for, flash
from jinja2 import TemplateNotFound
import flask.ext.security as security
from models.Candidate import Candidate
from models.PolicyArea import PolicyArea
from models.PanelQuestionaire import PanelQuestionaire
from models.PanelResponse import PanelResponse
from app import db

bp = Blueprint('panel', __name__, template_folder='templates')



class PanelController():

    def get_questionaire_for(self, user):
        questionaire = PanelQuestionaire.query.filter_by(panel_member = user).first()
        
        if questionaire == None:
            questionaire = PanelQuestionaire(
                                    panel_member = user)

            db.session.add(questionaire)
            db.session.commit()

        return questionaire

    def get_confirmed_candidates(self):
        return Candidate.query.filter_by(confirmed=True).order_by(Candidate.name).all()



class Stats:
    def __init__(self):
        pass

    def trivia(self, party_name, policy_name):
        candidate = Candidate.query.filter_by(name = party_name).first()
        policies = PolicyArea.query.filter_by(name = policy_name).first()
        sentiment = PanelResponse.query.filter_by(policy_area_id=policies.id, candidate_id=candidate.id).all()        
        
        return {
            "name": candidate.name,
            "sentiment": self.mean(sentiment),
            "policy": policies.name
        }

    def mean(self, ls):
        val = []
        
        for sent in ls:
            val.append(sent.value)

        if len(val):
            return self.avg(val)

        return 0

    def avg(self, l):
        if l:
            return sum(l) / len(l)
        return 0


panelController = PanelController()



@bp.route('/')
@security.login_required
@security.roles_accepted('panel')
def index():
    policies = PolicyArea.query.all()
    q = panelController.get_questionaire_for(security.current_user)
    candidates = panelController.get_confirmed_candidates()

    stats = Stats()

    return render_template('panel/index.j2',
                    policy_areas = policies,
                    stats = [stats.trivia("Greens", "Education"), stats.trivia("National", "Healthcare")]
                )
    


@bp.route('/policy/<policy_id>', methods=['GET', 'POST'])
@security.login_required
@security.roles_accepted('panel')
def policy(policy_id):
    q = panelController.get_questionaire_for(security.current_user)
    candidates = panelController.get_confirmed_candidates()
    policy = PolicyArea.query.filter_by(id=policy_id).first_or_404()
    existing_responses = q.responses.filter_by(policy_area_id=policy_id).all()

    if request.method == 'POST':

        # Really dirty form checking
        for (field, val) in request.form.items():

            if field.startswith("csrf"):
                continue

            candidate_id = int(field.replace("candidate[","").replace("]", ""))

            didUpdate = False
            
            for existing in existing_responses:
                if existing.candidate_id == candidate_id:
                    existing.value = val
                    didUpdate = True

            if not didUpdate:
                response = PanelResponse(
                                policy_area_id = policy_id,
                                candidate_id = candidate_id, 
                                panel_questionaire_id = q.id, 
                                value = val )

                db.session.add(response)

        db.session.commit()

        flash('Saved your selection for %s ' % (policy.name))

        return redirect(url_for('.index'))

    else:
        for candidate in candidates:
            for res in existing_responses:
                if res.candidate_id == candidate.id:
                    print res.value
                    candidate.response = res

        return render_template('panel/edit.j2', candidates = candidates, policy = policy)






