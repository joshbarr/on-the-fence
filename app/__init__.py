import sqlite3
import os
import datetime
import time
import shutil
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, json, escape, make_response, send_from_directory, after_this_request
from flask.ext.sqlalchemy import SQLAlchemy
from wtforms import Form, BooleanField, TextField, TextAreaField, validators   
import flask_wtf
from flask.ext.mail import Mail, Message
from flask.ext.admin import Admin
from flask.ext.security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required

import flask.ext.security as flask_security
from hashids import Hashids

# Load app config
app = Flask(__name__)
app.config.from_pyfile("../config/settings.py")

db = SQLAlchemy(app)
flask_wtf.CsrfProtect(app)
mail = Mail(app)
app.hashids = Hashids(salt=app.config["HASHID_SALT"],  min_length=8)

from models import *
from routes import *
from auth import login as flask_login

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User.User, Role.Role)
security = Security(app, user_datastore)

# Register sub-apps
import panel as panel
import game as game
import admin as site_admin

app.register_blueprint(panel.bp, url_prefix='/Q')
app.register_blueprint(game.bp, url_prefix='/play')


# Create a user to test with
@app.before_first_request
def create_user():
    db.drop_all()
    db.create_all()

    # Create some initial roles
    role_admin = user_datastore.create_role(name="admin", description="Site administratior");
    role_editor = user_datastore.create_role(name="editor", description="Site editor");
    role_panel = user_datastore.create_role(name="panel", description="Panel member");

    # Set up users
    user_boss = user_datastore.create_user(
        email="boss@onthefence.co.nz",
        password= flask_security.utils.encrypt_password("admin"),
        first_name="Kieran",
        last_name="Stowers"
        )

    user_admin = user_datastore.create_user(
        email="admin@onthefence.co.nz",
        password= flask_security.utils.encrypt_password("super"),
        first_name="Josh",
        last_name="Barr"
        )

    user_panel = user_datastore.create_user(
        email="panel@onthefence.co.nz",
        password= flask_security.utils.encrypt_password("panel"),
        first_name="Random",
        last_name="Pannelist"
        )

    # Bind up some basic roles...
    user_datastore.add_role_to_user(user_admin, role_admin)
    user_datastore.add_role_to_user(user_admin, role_panel)
    user_datastore.add_role_to_user(user_admin, role_editor)

    user_datastore.add_role_to_user(user_boss, role_admin)
    user_datastore.add_role_to_user(user_panel, role_panel)



    # Add some sample policy areas
    db.session.add(PolicyArea.PolicyArea(name="Education"));
    db.session.add(PolicyArea.PolicyArea(name="Healthcare"));
    db.session.add(PolicyArea.PolicyArea(name="Arts"));
    db.session.add(PolicyArea.PolicyArea(name="Welfare"));
    db.session.add(PolicyArea.PolicyArea(name="Housing"));
    db.session.add(PolicyArea.PolicyArea(name="Government"));

    bias_left = Bias.Bias(name="extreme_left",x_coordinate=-100, description="The left-most position");

    db.session.add(bias_left)
    db.session.add(Bias.Bias(name="extreme_right",x_coordinate=100, description="The right-most position"));
    db.session.add(Bias.Bias(name="neutral", x_coordinate=0, description="Exactly in the middle"));

    db.session.add(Statement.Statement(text="There's advantages to both", policy_area_id = 1, bias_id = 1))
    db.session.add(Statement.Statement(text="advantages, advantages!", policy_area_id = 1, bias_id = 2))

    db.session.add(Candidate.Candidate(name="National", url="https://www.national.org.nz/", confirmed=1));
    db.session.add(Candidate.Candidate(name="Labour", url="https://www.labour.org.nz/",  confirmed=1));
    db.session.add(Candidate.Candidate(name="Greens", url="https://www.greens.org.nz/", confirmed=1));
    db.session.add(Candidate.Candidate(name="ACT", url="https://www.act.org.nz/"));
    db.session.add(Candidate.Candidate(name="Internet-Mana", url="https://www.internet-mana.org.nz/",  confirmed=1));
    db.session.add(Candidate.Candidate(name="Maori", url="https://www.maori.org.nz/",  confirmed=1));
    db.session.add(Candidate.Candidate(name="NZ First", url="https://www.maori.org.nz/"));
    db.session.add(Candidate.Candidate(name="Conservative", url="https://www.maori.org.nz/"));
    db.session.add(Candidate.Candidate(name="United Future", url="https://www.maori.org.nz/"));

    # Save test data
    db.session.commit()
    return

if __name__ == '__main__':
    app.run(debug = True)