$("document").ready(function() {

    var $sliders = $("[data-slider]");



    $sliders.each(function() {
        var startVal =  0.5;
        var dataVal = $(this).data("slider");
        var $this = $(this);
        var $el = $this.siblings("[data-slider-input]").first()

        if (dataVal) {
            startVal = dataVal;
        }

        $this.noUiSlider({
            start: startVal * 100,
            range: {
                'min': 0,
                'max': 100
            },
            serialization: {
                lower: [
                    $.Link({
                        target: function( value, handleElement, slider ) {
                            var val = value / 100;
                            $el.val( val.toFixed(2) );
                        }
                    })
                ]
            }
        });
    });

});
