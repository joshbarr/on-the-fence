# Database
# SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'


# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))  

# Define the database - we are working with
# SQLite for this example
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'var/app.db')
DATABASE_CONNECT_OPTIONS = {}

print SQLALCHEMY_DATABASE_URI

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "electricfence777"

# Secret key for signing cookies
SECRET_KEY = "ipeedonthefenceandithurt"


SECURITY_PASSWORD_HASH = "bcrypt"
SECURITY_PASSWORD_SALT = "salty"

# Security routes
SECURITY_POST_LOGIN_VIEW = "/"
SECURITY_POST_LOGOUT_VIEW = "/sign-out"
SECURITY_UNAUTHORIZED_VIEW = "/sign-in"
UNAUTHORIZED_VIEW = "/sign-in"
SECURITY_LOGIN_URL = "/sign-in"
SECURITY_REGISTER_URL = "/register"
SECURITY_RESET_URL = "/reset"
SECURITY_CHANGE_URL = "/change"
SECURITY_CONFIRM_URL = "/confirm"


SECURITY_TRACKABLE = True
SECURITY_REGISTERABLE = True
# SECURITY_CONFIRMABLE = True
SECURITY_RECOVERABLE = True



SECURITY_DEFAULT_REMEMBER_ME = True


# Email setup
MAIL_SERVER = 'smtp.example.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'username'
MAIL_PASSWORD = 'password'



HASHID_SALT = "salty seafaring sailor"